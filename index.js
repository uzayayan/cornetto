var targetHashtag = '#istanbul';
var maximumCount = 100;

//#region FIREBASE_API
var admin = require("firebase-admin");
var serviceAccount = require("./algida-273d8-firebase-adminsdk-n89yl-ea0c1249ac.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://algida-273d8.firebaseio.com"
});
var db = admin.database();
var usersRef = db.ref().child("users");
var expiredRef = db.ref().child("expired");

//#endregion
//#region TWITTER_API 
var Twitter = require('twitter');
var client = new Twitter({
    consumer_key: 'kTBJg303xoKrTqkEOX05uwqir',
    consumer_secret: 'foT2GwsNuIO2mR0zMyH95Vu7sk9e53CcD7QoFV9tshaRF97tEL',
    access_token_key: '3894366137-kIbBPghlKV26qQMfJ4kuNsQpWHZxlDc94cFTeG6',
    access_token_secret: 'cP0tCxkUonDH8Abdf8ng2ClIahqvyGCeBe6nEg8PEhsqL'
});
//#endregion

//#region SYNCHRONOUS
syncTwitterData();
function syncTwitterData(){
    client.get('https://api.twitter.com/1.1/search/tweets.json', {q: targetHashtag,count:maximumCount},  function(error, tweets, response){
        if(error) throw error;
        parseDataFromSync(response.body);
    }); 
}

function parseDataFromSync(data) {

        var jsonContent = JSON.parse(data);
        var dataArray = Array.from(jsonContent.statuses);
        var forEachAsync = require('forEachAsync').forEachAsync;
        forEachAsync(dataArray, function (next, element, index, array) {
            filter(element.text).then(function(resolve){

                if(resolve == true){

                    var data = {
                        'ID':element.id,
                        'username':element.user.screen_name,
                        'name':element.user.name,
                        'text':element.text,
                        'Time':element.created_at};

                    sendToDataBaseFromSync(element.id,data);
                }
                next();
            });
        });
}

function sendToDataBaseFromSync(ID, data){

    db.ref("users").child(ID).once("value").then(function(snapshot) {
        if(!snapshot.exists())
            usersRef.child(ID).set(data,function(error){
                if(error)
                    errorHandle(error)

                console.log("Username : " + data.username + " Tweet : " + data.text);
            });                            
    });
}
//#endregion

//#region STREAMING
streamTwitterData();
function streamTwitterData(){

    try
    {
        client.stream('statuses/filter', {track: targetHashtag},  function(stream) {
            stream.on('data', function(tweet) {
              parseDataFromStream(tweet)
            });
          
            stream.on('error', function(error) {
              errorHandle(error);
            });
          });
    }
    catch(error)
    {
        errorHandle(error);
    }
}

function parseDataFromStream(data){

    try
    {
        var jsonContent = data;

        filter(jsonContent.text).then(function(resolve){
            if(resolve == true){
                var data = {
                    'ID':jsonContent.id,
                    'username':jsonContent.user.screen_name,
                    'name':jsonContent.user.name,
                    'text':jsonContent.text,
                    'Time':jsonContent.created_at};

                sendToDataBaseFromStream(jsonContent.id,data);
            }
        });
    }
    catch(error)
    {
        errorHandle(error);
    }
}

function sendToDataBaseFromStream(ID, data){

    usersRef.child(ID).set(data,function(error){
        if(error)
            errorHandle(error)

        console.log("Username : " + data.username + " Tweet : " + data.text);
    });   
}
//#endregion

//#region FILTERING PROCESSES
function filter(data) {

    return new Promise(function(resolve, reject){
        try
        {
            var pattern = new RegExp(targetHashtag, 'gi');
            var rawData = data.replace(pattern,'').replace(/\s/g, ''); // CLEANING DATA

            if(rawData != '')
            {
                var sampleArray = [...rawData]; // SEPARATING DATA
                var forEachAsync = require('forEachAsync').forEachAsync;

                forEachAsync(sampleArray, function (next, element, index, array) {
                    if(!isEmoji(element)){
                        resolve(false);
                        return;
                    }
                    next();
                }).then(function () {
                    resolve(true);
                });
            }
            else
            {
                resolve(false);
                return;
            }
        }
        catch(error){
            errorHandle(error);
        }
    })
}

function isEmoji(data) { //TODO: IMPROVE IT

    var ranges = ['/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g'];
    if (data.match(ranges.join('|')))
        return true;

    return false;
}
//#endregion

//#region HANDLES
function errorHandle(error){
    console.log(error);
}
//#endregion

//#region EXPRESS
var express = require('express');
var http = express();
 
http.get('/reply', function (req, res) {
    req.on('data', data => {
        
        var parsedData = JSON.parse(data);

        replyTweet(parsedData.message,parsedData.tweetID).then(function(resolve){
            res.statusCode = 200;
            res.end();
        })
        .catch(function(reject){
            res.statusCode = 400;
            res.end();
        });
    });
});

http.get('/nexttweet', function(req, res){

    getNextTweet().then(function(resolve){
        res.statusCode = 200;
        res.write(JSON.stringify(resolve));
        res.end();
    })
    .catch(function(reject){
        res.statusCode = 400;
        res.end();
    });
});

http.listen(8000);
//#endregion

//#region EXPRESS OPERATIONS
function replyTweet (message, targetTweetID) {

    return new Promise(function(resolve, reject){

        client.post('statuses/update', {status: message, in_reply_to_status_id: targetTweetID}).then(function (tweet) {
            
            expiredRef.child(targetTweetID).set(message, function(error){
                errorHandle(error);
                reject();
            });

            resolve();
        })
        .catch(function (error) {
            errorHandle(error);
            reject();
        })
    })
}

function getNextTweet(){
    
    return new Promise(function(resolve, reject){

        usersRef.limitToFirst(1).once('value', function(snap){

            const ID = Object.keys(snap.val())[0];
            usersRef.child(Object.keys(snap.val())[0]).remove();
            resolve(snap.val());
        })
        .catch(function (error){
            errorHandle(error);
            reject();
        })
    });
}
//#endregion
                         g